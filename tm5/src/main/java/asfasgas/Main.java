package asfasgas;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

	private static List<MonitoredData> activities = new ArrayList<MonitoredData>();
	private Scanner x;

	public void file() {
		try {
			x = new Scanner(new File("Activity.txt"));
		} catch (Exception e) {
			System.out.println("could not find file");
		}
		while (x.hasNext()) {
			String s1 = x.next();
			String s2 = x.next();
			String s3 = x.next();
			String s4 = x.next();
			String s5 = x.next();
			
			activities.add(new MonitoredData(s1, s2, s3, s4, s5));
		}
		x.close();
	}

	public void nrDays() {
		List<String> startDates = activities.stream().map(MonitoredData::getStartDate).distinct()
				.collect(Collectors.toList());
		List<String> endDates = activities.stream().map(MonitoredData::getEndDate).distinct()
				.collect(Collectors.toList());

		startDates.addAll(endDates);
		System.out.println("Distinct Dates: " + startDates.stream().distinct().count());
	}

	public void occureneces() {
		Map<String, Long> counters = activities.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));
		try {
			PrintWriter out = new PrintWriter(new File("ActivitiesAppearances.txt"));
			for (Entry<String, Long> entry : counters.entrySet()) {
				out.println(entry.getKey() + "\t=>\t" + entry.getValue());
			}
			out.close();
		} catch (IOException exc) {
			System.out.println("Fail!");
		}
	}

	public void occurencebyday() {
		Map<Object, Map<String, Long>> ceva = activities.stream()
				.sorted((e1, e2) -> e1.startDate.compareTo(e2.startDate))
				.collect(Collectors.groupingBy(p -> p.getStartDate(),
						Collectors.groupingBy(p -> p.getActivityLabel(), Collectors.counting())));
		
		/*try {
			taos= new TextAreaOutputStream( ta, 60 );
		} catch (Exception e1) {

			e1.printStackTrace();
		}
		ps= new PrintStream( taos );
		System.setOut( ps );
		System.setErr( ps );
		
		setSize(500,500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setContentPane(backPanel);
		
		ButtonGroup group = new ButtonGroup();
		group.add(simpleSearchButton);
		group.add(advancedSearchButton);
		
		operationsPanel.setLayout(new GridLayout(6,2));
		
		operationsPanel.add(addWordButton);
		operationsPanel.add(saveButton);
		operationsPanel.add(searchButton);
		operationsPanel.add(displayButton);
		operationsPanel.add(deleteWordButton);
		
		consolePanel.setLayout(new GridLayout(1,1));
		sbrText = new JScrollPane(ta);
		consolePanel.add(sbrText);
		backPanel.setLayout(new GridLayout(2,1));
		backPanel.add(operationsPanel);
		backPanel.add(consolePanel);
		addListeners();
		addWindowListener();
		dictionary.restoreSynonyms();*/
	
	}

	public void totalDuration() {/*
		private void addWindowListener(){
			setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			addWindowListener(new WindowAdapter(){
				public void windowClosing(WindowEvent e)
				{
					if(!saved)  displayConfirmExitDialog();
					else System.exit(0);
				}
			});
		}
		private void saveModifications(){
			dictionary.saveSynonyms();
			saved=true;
		}
		
		private void displayConfirmExitDialog(){
			int reply =JOptionPane.showConfirmDialog(null, "Do you want to save the modifications ?","Confirm exit",JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.NO_OPTION)
			{
				System.exit(0);
			}
			if (reply == JOptionPane.YES_OPTION)
			{
				saveModifications();
				System.exit(0);
			}
		}*/
	}

	public void filterActivities() {/*
		public void actionPerformed (ActionEvent e){
				String s=e.getActionCommand();
				if (s.equals("Delete a word")){
					deleteWordDialog = new JDialog();
					deleteWordDialog.setSize(300,200);
					deleteWordDialog.setLayout(new GridLayout(5,2));
					deleteWordDialog.add(wordSearchLabel);
					deleteWordDialog.add(deleteWordField);
					
					
					deleteWordDialog.add(deleteOk);
					deleteWordDialog.setVisible(true);
					}
					
				else if (s.equals("Delete")){
					if (deleteWordField.getText().isEmpty()) JOptionPane.showMessageDialog(deleteWordDialog,"Please enter all values!");
					else {
						
						dictionary.removeDefinition(deleteWordField.getText());						
						saved=false;
						JOptionPane.showMessageDialog(deleteWordDialog,"The word "+deleteWordField.getText().toUpperCase()+" has been deleted!");
					}
				}
				else if (s.equals("New word")){

					addWordDialog = new JDialog();
					addWordDialog.setSize(300,200);
					addWordDialog.setLayout(new GridLayout(5,2));
					addWordDialog.add(newWordLabel);
					addWordDialog.add(wordField);
					addWordDialog.add(synonymLabel);
					addWordDialog.add(synonymField);
					
					addWordDialog.add(addWordOk);
					addWordDialog.setVisible(true);

				}
				else if (s.equals("Add word")){
					if (wordField.getText().isEmpty()||synonymField.getText().isEmpty()) JOptionPane.showMessageDialog(addWordDialog,"Please enter all values!");
					else {
						
						dictionary.addSynonym(wordField.getText(), synonymField.getText());					
						saved=false;
						JOptionPane.showMessageDialog(addWordDialog,"The word "+wordField.getText().toUpperCase()+" and the synonym "+synonymField.getText().toUpperCase() +" has been added ");
					}
				}
				else if (s.equals("Search synonym")){
					searchDialog = new JDialog();
					searchDialog.setSize(300,200);
					searchDialog.setLayout(new GridLayout(5,2));
					searchDialog.add(wordSearchLabel);
					searchDialog.add(searchField);
					searchDialog.add(simpleSearchButton);
					searchDialog.add(advancedSearchButton);
					
					searchDialog.add(searchOk);
					searchDialog.setVisible(true);
				}
				else if (s.equals("Search")){
					if (searchField.getText().isEmpty()) JOptionPane.showMessageDialog(searchDialog,"Please enter a word!");
					else {
						if (!okAdvancedSearch)
						dictionary.simpleSearch(searchField.getText());
						else dictionary.advancedSearch(searchField.getText());
					}
					
					}
				else if (s.equals("Save")){
					dictionary.saveSynonyms();
					saved=true;
				}
				else if (s.equals("Simple search")){
					okAdvancedSearch=false;
				}
				else if (s.equals("Advanced search")){
					okAdvancedSearch=true;
				}
				else if (s.equals("Display")){
					dictionary.display();
				}
				}
				
			}
	*/}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Main d = new Main();
		d.file();
		d.nrDays();
		d.occureneces();
		d.occurencebyday();
		d.totalDuration();
		d.filterActivities();
	}

}
